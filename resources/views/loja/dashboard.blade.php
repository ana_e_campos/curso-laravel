@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')

<div class="col-md-10 offset-md-1 dashboard-title-container">
    <h1>Minhas lojas</h1>
</div>

<div class="col-md-10 offset-md-1 dashboar-title-constainer">
    @if (count($lojas) > 0)
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Ações</th>
                </tr>
            </thead>
        <tbody>
            @foreach ($lojas as $loja)
                <tr>
                    <th scropt="row">{{ $loop->index + 1 }}</th>
                    <td><a href="/lojas/{{ $loja->nome }}">{{ $loja->nome }}</a></td>
                    <td><a href="#"> Editar</a><a href="#">Deletar</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
        <p>Você ainda não possui lojas cadastradas, <a href="/lojas/create">criar loja</a></p>
    @endif
</div>

@endsection
