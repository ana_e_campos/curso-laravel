@extends('layouts.main')

@section('title', 'Criar loja')

@section('content')

<div id="loja-create-container" class="col-md-6 offset-md-3">
    <h1>Crie uma loja</h1>
    <form action="/lojas" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="imagem">Imagem</label>
            <input type="file" class="form-control-file" id="imagem" name="imagem">
        </div>
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="">
        </div>
        <div class="form-group">
            <label for="filial">Filial</label>
            <input type="text" class="form-control" id="filial" name="filial" placeholder="">
        </div>
        <div class="form-group">
            <label for="endereco">Endereço</label>
            <input type="text" class="form-control" id="endereco" name="endereco" placeholder="">
        </div>
        <div class="form-group">
            <label for="telefone">Telefone</label>
            <input type="text" class="form-control" id="telefone" name="telefone" placeholder="">
        </div>
        <div class="form-group">
            <label for="gerente">Gerente</label>
            <input type="text" class="form-control" id="gerente" name="gerente" placeholder="">
        </div>
        <div class="form-group">
            <label for="data">Data</label>
            <input type="date" class="form-control" id="data" name="data" placeholder="">
        </div>
        <div class="form-group">
            <label for="titile">Adicione a Região:</label>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="Grande Florianópolis"> Grande Florianópolis
            </div>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="Vale do Itajaí"> Vale do Itajaí
            </div>
            <div class="form-group">
                <input type="checkbox" name="items[]" value="Norte"> Norte
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-success" id="salvarloja" name="salvarloja">Salvar</button> 
        </div>
    </form>
</div>
@endsection
