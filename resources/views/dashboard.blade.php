@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')

<div class="col-md-10 offset-md-1 dashboard-title-container">
    <h1>Minhas lojas</h1>
</div>

<div class="col-md-10 offset-md-1 dashboar-title-constainer">
    @if (count($lojas) > 0)

    @else
        <p>Você ainda não possui lojas cadastradas, <a href="/lojas/create">criar loja</a></p>
    @endif
</div>

@endsection
