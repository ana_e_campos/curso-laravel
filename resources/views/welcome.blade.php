@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')

    <div id="search-container" class="col-md-12">
        <h1>Busque uma loja</h1>
        <form action="/" method="GET">
            <input type="text" id="search" name="search" class="form-control">
        </form>
    </div>
    <div id="lojas-container" class="col-md-12">
        <h2>Lojas</h2>
        @if ($search)
            <p>Resultado da sua busca: {{ $search }}</p>
        @else
            <p>Veja todas as nossas lojas</p>
        @endif
        <div id="cards-container" class="row">
            @foreach ($lojas as $loja)
                <div class="col-md-3" id="loja">
                    <div class="card">
                        <img src="/img/loja/{{ $loja->imagem }}" class="card-img-top" alt="{{ $loja->nome }}">
                        <div class="card-body">
                            <p class="card-date">{{ date('d/m/Y', strtotime($loja->data)) }}</p>
                            <h5 class="card-title">{{ $loja->nome }}</h5>
                            <p class="card-dados">{{ $loja->telefone }}</p>
                            <a href="/lojas/{{ $loja->id }}" class="btn btn-danger">Saiba mais</a>
                        </div>
                    </div>
                </div>
            @endforeach
            @if (count($lojas) == 0)
                <p>Não há lojas cadastradas</p>
            @endif
        </div>
    </div>
    
 <!--comentário aparece no console-->
 {{-- este comentário não aparece no Blade --}}
@endsection