<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonte do Google-->
        <link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">

        <!-- CSS Bootstrap-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <!-- CSS Aplicação -->
        <link rel="stylesheet" href="/css/styles.css">

    </head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="collapse navbar-collapse" id="navbar">
                <a href="/" class="navbar-brand">
                    <img src="/img/logo.png" alt="Casas da Água">
                </a>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="/lojas" class="nav-link">Lojas</a>
                    </li>
                    @auth
                    <li class="nav-item">
                        <a href="/loja/create" class="nav-link">Criar loja</a>
                    </li>
                    @endauth
                    @auth
                    <li class="nav-item">
                        <a href="/dashboard" class="nav-link">Meus dados</a>
                    </li>
                    <li class="nav-item">
                        <form action="/logout" method="POST">
                            @csrf
                            <a href="/logout" class="nav-link"
                            onclick="loja.preventDefault();
                            this.closest('form').submit();">Sair</a>
                        </form>
                    </li>
                    @endauth
                    @guest
                    <li class="nav-item">
                        <a href="/login" class="nav-link">Entrar</a>
                    </li>
                    <li class="nav-item">
                        <a href="/register" class="nav-link">Cadastrar</a>
                    </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </header>
    <main>
        <div class="container-fluid">
            <div class="row">
                 @if(session('msg'))
                 <div id="mensagem" class="alert alert-success" role="alert">
                     <p class="msg">{{ session('msg') }}</p>
                 </div>
                 @endif
                 @yield('content')
            </div>
        </div>
    </main>
  <footer>
      <p>Ana Campos &copy; 2020</p>
  </footer>
        <!-- JS Aplicação -->
        <script src="/js/script.js"></script>

        <!-- JS Icons -->
        <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
</body>
</html>

