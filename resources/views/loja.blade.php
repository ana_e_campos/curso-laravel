@extends('layouts.main')

@section('title', 'Lojas')

@section('content')
<h1>Página de lojas</h1>
    @foreach ($lojas as $loja)
        <p>{{ $loja->nome }} - {{ $loja->telefone }}</p>
    @endforeach
@endsection