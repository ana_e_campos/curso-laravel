<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\LojaController;
use App\Http\Controllers\RegiaoController;

/* GET */
Route::get('/', [LojaController::class, 'index']);
Route::get('/lojas', [LojaController::class, 'lojas']);
Route::get('/lojas/{id}', [LojaController::class, 'show']);
Route::get('/loja/create', [LojaController::class, 'create'])->middleware('auth');

/* POST */
Route::post('/lojas', [LojaController::class, 'store']);

Route::get('/dashboard', [LojaController::class, 'dashboard1'])->middleware('auth');

