<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loja;
use App\Models\User;

class LojaController extends Controller
{
    public function index(){

        $search = request('search');

        if($search){

            $lojas = Loja::where([
                ['nome', 'like', '%'.$search.'%']
            ])->get();

        }
        else{
            $lojas = Loja::all();
        }

        return view('welcome', ['lojas' => $lojas, 'search' => $search]);

    }

    public function lojas(){

        $lojas = Loja::all();

        return view('loja', ['lojas' => $lojas]);

    }

    public function create(){
        return view('loja.create');
    }

    public function store(Request $request){
        $loja = new Loja;
        $loja->nome = $request->nome;
        $loja->filial = $request->filial;
        $loja->endereco = $request->endereco;
        $loja->telefone = $request->telefone;
        $loja->gerente = $request->gerente;
        $loja->items = $request->items;
        $loja->data = $request->data;

        //image upload
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){

            $requestImage = $request->imagem;

            $extension = $requestImage->extension();

            $imageName = md5($requestImage->getClientOriginalName() . strtotime("now")) . "." . $extension;

            $request->imagem->move(public_path('img/loja'), $imageName);

            $loja->imagem = $imageName;
        }

        $user = auth()->user();

        $loja->user_id = $user->id;

        $loja->save();

        return redirect('/')->with('msg', 'Loja criada com sucesso!');

    }

    public function show($id){

        $loja = Loja::findOrFail($id);

        $lojaOwner = User::where('id', $loja->user_id)->first()->toArray();

        return view('loja.show', ['loja' => $loja, 'lojaOwner' => $lojaOwner]);

    }

    public function dashboard1(){

        $user = auth()->user();

        $lojas = $user->lojas;

        return view('loja.dashboard', ['lojas' => $lojas]);

    }
}
